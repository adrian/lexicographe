import re

AUTHOR = 'Les lexicos'
SITENAME = 'Lexicographe Déconstruit'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Rome'

DEFAULT_LANG = 'fr'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

THEME = './themes/default/'

DEFAULT_DATE = 'fs'

# Blogroll
LINKS = (('Pelican', 'https://getpelican.com/'),
         ('Python.org', 'https://www.python.org/'),
         ('Jinja2', 'https://palletsprojects.com/p/jinja/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = False

# custom Jinja2 filter for indexing articles
def sort_by_letter(articles):
    organized_articles = {}
    for article in sorted(articles, key=lambda a:a.title):
        if article.category != 'definitions':
            continue
        letter = article.title[0].upper()
        if letter not in organized_articles:
            organized_articles[letter] = []
        organized_articles[letter].append(article)
    return organized_articles.items()

def titleless (html):
    """Filter out titles in html"""
    return re.sub('<h[0-9]>[^<]*</h[0-9]>', ' ', html)

def styleless (html):
    """Filter out html tags"""
    return re.sub('<[^<]*>', ' ', html)

def begin (text):
    """Only let the begining of the text"""
    return text if len(text) < 200 else text[0:200] + '…'

JINJA_FILTERS = {
    "sortbyletter": sort_by_letter,
    "titleless": titleless,
    "styleless": styleless,
    "begin": begin,
}

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True
